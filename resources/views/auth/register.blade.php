@extends('layouts.main-app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="my-text">
        <h2>Register Not Allowed.<br>Contact Admin !!!</h2>
        <div>
            <a href="/"><button class="btn btn-primary btn-arrow-left">Go Back</button></a>
        </div>
    </div>
    </div>
</div>
@endsection
