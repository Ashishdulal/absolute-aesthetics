@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog_responsive.css')}}">
<style type="text/css">
6. Blog
*********************************/

.blog
{
	background: #FFFFFF;
	padding-top: 91px;
	padding-bottom: 83px;
}
.blog-detail{
	padding-top: 91px;
    padding-bottom: 83px;
}
.blog_post:not(:last-child)
{
	margin-bottom: 87px;
}
.blog_post_image
{
	width: 100%;
	height: 300px;
	overflow: hidden;
}
.blog_post_image-my
{
	width: 100%;
	border-radius: 40px;
	top: 40px;
	overflow: hidden;
}
.blog_post_text.text-center p {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    line-height: 16px;
    max-height: 32px;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}
.text-center-des{
	text-align: justify;
	padding: 30px 0px;
}
.blog_post_image img
{
	max-width: 100%;

}
.blog_post_date
{
	position: absolute;
	top: 8px;
	left: 9px;
	width: 90px;
	height: 118px;
	background-color: #57ccc3;
	border-radius: 40px;
	border: solid 2px #eaf2f5;
}
.blog_post_date
{
	font-weight: 600;
	color: #FFFFFF;
	line-height: 0.75;
}
.date_day
{
	font-size: 42px;
}
.date_month,
.date_year
{
	font-size: 12px;
}
.date_month
{
	margin-top: 10px;
}
.date_year
{
	margin-top: 11px;
}
.blog_post_title
{
	text-align: center;
	margin-top: 45px;
	white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.blog_post_title a
{
	font-size: 30px;
	font-weight: 600;
	color: #404040;
	line-height: 1.2;
}
.blog_post_title a:hover
{
	color: #57ccc3;
}
.blog_post_info
{
	margin-top: 9px;
}
.blog_post_info ul li:not(:last-child)
{
	margin-right: 24px;
}
.blog_post_info ul li,
.blog_post_info ul li a
{
	font-size: 14px;
	font-weight: 600;
	color: #868686;
}
.blog_post_info ul li a
{
	color: #57ccc3;
}
.blog_post_info ul li a:hover
{
	color: #fd556d;
}
.blog_post_text
{
	margin-top: 32px;
}
.blog_post_button
{
	margin-top: 38px;
	margin-bottom: 38px;
}
.blog_post_button > div
{
	display: inline-block;
}
.page_nav
{
	margin-top: 85px;
}
.page_nav ul li a
{
	font-size: 16px;
	font-weight: 600;
	color: #404040;
}
.page_nav ul li:not(:last-child)
{
	margin-right: 10px;
}
.page_nav ul li.active a,
.page_nav ul li a:hover
{
	color: #57ccc3;
}
.home{
	height: 400px !important;
}
/*********************************
</style>
@section('content')
<!-- Home -->

	<div class="home d-flex flex-column align-items-start justify-content-end">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/uploads/{{$blogs->f_image}}" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="my-title-blog">
							<div style="text-transform: uppercase;font-size: 35px; color: #ffff;" class="home_title">{{$blogs->title}}</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- Blog -->

<div style="background: #fff;" class="blog">
	<div class="container">
		<div class="row">
			<div class="col">

				<!-- Blog Post -->
				<div class="blog_post">
					<div class="row">
					<div class="col-sm-6 blog_post_image-my"><img style="max-width: 100%;" src="/uploads/{{$blogs-> f_image}}" alt="{{$blogs->name}}"></div>
					<br>
					<div class="col-sm-6 blog_post_image-my"><img style="max-width: 100%;" src="/uploads/{{$blogs-> i_image}}" alt="{{$blogs->name}}"></div>
				</div>
</div>					<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
						<div class="date_day">{{ $blogs->created_at->format('d') }}</div>
						<div class="date_month">{{ $blogs->created_at->format('M') }}</div>
						<div class="date_year">{{ $blogs->created_at->format('Y') }}</div>
					</div>
					<div class="blog_post_info">
						<ul class="d-flex flex-row align-items-center justify-content-center">
							<li>By : <a >Admin</a></li>
							<li>In : <a >
								@foreach($blogcategories as $blogcategory)
								@if($blogs->cat_id === $blogcategory->id)
								{{$blogcategory->title}}
								@endif
								@endforeach
							</a></li>
						</ul>
					</div>
					<div class="blog_post_title"><a style="color: #000000;" >{{$blogs->title}}</a></div>	
					<div class=" text-center-des">
						<p><?php echo ($blogs->description)?></p>
					</div>

				</div>

			</div>
		</div>
	</div>

</div>





@endsection