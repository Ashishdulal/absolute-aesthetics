<!-- Header -->

<header class="header trans_400">
	<div class="header_content d-flex flex-row align-items-center jusity-content-start trans_400">
		<!-- Logo -->
		<div class="logo">
			<a href="/">
				<img src="{{asset('images/absolute-aesthetics.jpg')}}" alt="logo">
			</a>
		</div>

		<!-- Main Navigation -->
		<nav class="main_nav">
			<ul class="d-flex flex-row align-items-center justify-content-start">
				<li class="nav-item"><a href="/">Home</a></li>
				<li class="nav-item"><a href="/about">About us</a></li>
				<li class="nav-item dropdown">
					<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Services
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						@foreach($service_header as $service)
						<a class="dropdown-item" href="/services-detail/{{ $service->id }}">{{ $service->name }}</a>
						@endforeach
					</div>
				</li>
				<li class="nav-item"><a href="/show-gallery">Gallery</a></li>
				<li class="nav-item"><a href="/blog">Blog</a></li>
				<li class="nav-item"><a href="/contact">Contact</a></li>
			</ul>
		</nav>
		<div class="header_extra d-flex flex-row align-items-center justify-content-end ml-auto">



			<!-- Header Phone -->
			<a class="header_phone" href="tel:+977 9841352751">+977-9840000121</a>

			<!-- Appointment Button -->
			<div data-toggle="modal" data-target="#myModal" class="button button_1 header_button"><a href="#">Make an Appointment</a></div>

			<!-- Header Social -->
			<div class="social header_social">
				<ul class="d-flex flex-row align-items-center justify-content-start">
					<li><a href="tel:+977-9840000121"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="https://www.instagram.com/absoluteaestheticsnepal/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="https://www.facebook.com/absoluteaestheticsnepal/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class="hamburger" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i></div>
		</div>

	</div>
	<!-- Hamburger -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light1 make-menu">

		<div class="collapse navbar-expand-lg" id="navbarNavAltMarkup">
			<div class="navbar-nav ">
				<a href="/">Home</a>
				<a href="/about">About us</a>
					<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Services
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						@foreach($service_header as $service)
						<a class="dropdown-item" href="/services-detail/{{ $service->id }}">{{ $service->name }}</a>
						@endforeach
					</div>
				<a href="/show-gallery">Gallery</a>
				<a href="/blog">Blog</a>
				<a href="/contact">Contact</a>
			</div>
		</div>
	</nav>
<div style="    background: #8f2a94;padding: 5px 0px 3px;text-transform: capitalize;" class="marquee">
	<MARQUEE behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
		@foreach ($blog_header as $blogs)
		@if($blogs->cat_id === 8)
		<i class="fa fa-hand-o-right white" style="color: #fff; font-weight: bold;" aria-hidden="true"></i>&nbsp;
<a href="/blog-detail/{{$blogs->id}}" style="color: #fff; font-weight: bold;">{{$blogs->title}}</a>
		<span class="spacer" style="padding: 0px 50px;"></span>
		@endif
		@endforeach
		 </marquee>
</div>
</header>
<!-- The Modal -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content make-other">

			<!-- Modal Header -->
			<div class="modal-header my-modal">
				<div class="intro_form_title">Make an Appointment</div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="col-lg-12 intro_col hov_form">
				@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
				<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
							@csrf()
							<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
								<input type="text" name="name" class="intro_select intro_input" placeholder="Your Name" required="required">
								<input type="email" name="email" class="intro_select intro_input" placeholder="Your E-mail" required="required">
								<input type="tel" name="phone" class="intro_select intro_input" placeholder="Your Phone" required="required">
								<select name="service" class="intro_select intro_input" required>
									<option disabled="" selected="" value="">Select Service</option>
									<option>Dental</option>
									<option>Dermatologist | Aesthetics</option>
								</select>
								<select name="doctor" class="intro_select intro_input" required="required">
							<option disabled="" selected="" value="">Select Doctor</option>
							<option>DR. LOKENDRA LIMBU</option>
							<option>DR. KHAGENDRA CHHETRI</option>
							<option>DR. SUNESHA PRADHAN</option>
							<option>DR. RAJEEV SINGH</option>
							<option>DR. RATNA BAHADUR GHARTI</option>
						</select>
						<input type="date" name="date" id="datepicker" class="intro_input datepicker hasDatepicker" placeholder="Date" required="required">
							</div>
							<button type="submit" class="button button_1 contact_button trans_200">make an appointment</button>
						</form>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
