	<!-- Footer -->

	<footer class="footer">
		<div class="footer_content">
			<div class="container">
				<div class="row">

					<!-- Footer About -->
					<div class="col-lg-3 footer_col">
						<div class="footer_about">
							<div class="footer_logo">
								<a href="#">
									<div>Absolute<span>Aesthetics</span></div>
									<div>Aesthetic Care</div>
								</a>
							</div>
							<div class="footer_about_text">
								<p>Flawless young and radiant skin along with a bright shining  smile Is a winning combination.</p>
							</div>
						</div>
					</div>

					<!-- Footer Contact Info -->
					<div class="col-lg-3 footer_col">
						<div class="footer_contact">
							<div class="footer_title">Contact Info</div>
							<ul class="contact_list">
								<li><a href="tel:+9779840000121">+977-9840000121</a><br>
								<a href="tel:+977015551176">+977-015551176</a></li>
								<li><a href="mailto:info@absoluteaesthetics.com.np">info@absoluteaesthetics.com.np</a></li>
								<li><a href="mailto:absoluteaestheticsnepal@hotmail.com">absoluteaestheticsnepal@hotmail.com</a></li>
							</ul><br>
							<div class="social ">
					<ul class="d-flex flex-row align-items-center justify-content-start">
						<li><a href="tel:+977-9840000121"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="https://www.instagram.com/absoluteaestheticsnepal/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="https://www.facebook.com/absoluteaestheticsnepal/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					</ul>
				</div>

						</div>
					</div>

					<!-- Footer Locations -->
					<div class="col-lg-3 footer_col">
						<div class="footer_location">
							<div class="footer_title">Our Location</div>
							<ul class="locations_list">
								<li>
									<div class="location_title">Shop No. 201, 2nd Floor</div>
									<div class="location_text">Labim Mall, Lalitpur</div>
								</li>
								<!-- <li>
									<div class="location_title">Bhaktapur</div>
									<div class="location_text">Balkot, Kausaltar</div>
								</li> -->
							</ul>
						</div>
					</div>

					<!-- Footer Opening Hours -->
					<div class="col-lg-3 footer_col">
						<div class="opening_hours">
							<div class="footer_title">Opening Hours</div>
							<ul class="opening_hours_list">
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Sunday:</div>									<div class="ml-auto">12:00pm - 8:00pm</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Monday:</div>
									<div class="ml-auto">12:00pm - 8:00pm</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Tuesday:</div>
									<div class="ml-auto">12:00pm - 8:00pm</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Wednesday:</div>
									<div class="ml-auto">12:00pm - 8:00pm</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Thursday:</div>
									<div class="ml-auto">12:00pm - 8:00pm</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Friday:</div>
									<div class="ml-auto">12:00pm - 8:00pm</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Saturday:</div>
									<div class="ml-auto">With Appointment</div>
								</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="footer_bar">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="footer_bar_content  d-flex flex-md-row flex-column align-items-md-center justify-content-start">
							<div class="copyright"><!-- Link back to nepgeeks can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script type="772b37959b40d44eaa8e7c82-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved | Made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://nepgeeks.com" style="color: #fff; font-size: 13px" target="_blank">NEPGEEKS TECHNOLOGY</a>
</div>
							<nav class="footer_nav ml-md-auto">
								<ul class="d-flex flex-row align-items-center justify-content-start">
									<li><a href="/">Home</a></li>
									<li><a href="/about">About us</a></li>
									<li><a href="/services">Services</a></li>
									<li><a href="/show-gallery">Gallery</a></li>
									<li><a href="/blog">Blogs</a></li>
									<li><a href="/contact">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
