@extends('layouts.main.main')

@section('content')

<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/blog.jpg" data-speed="0.8"></div>
	<div class="home_overlay"><img src="images/home_overlay.png" alt=""></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title">Contact</div>
						<div class="home_text">Our aim Is to give you the flawless skin that you desire.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Contact -->

<div class="contact">
	<div class="container">
		<div class="row">

			<!-- Contact Form -->
			<div class="col-lg-6">
				<div class="contact_form_container">
					<div class="contact_form_title">Make an Appointment</div>
					@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					@if ($message = Session::get('success'))
					<div class="alert alert-success alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $message }}</strong>
					</div>
					@endif
					<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
						@csrf()
						<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
							<input type="text" name="name" class="contact_input" placeholder="Your Name" required="required">
							<input type="email" name="email" class="contact_input" placeholder="Your E-mail" required="required">
							<input type="tel" name="phone" class="contact_input" placeholder="Your Phone" required="required">
							<select name="service" class="intro_select intro_input" required>
								<option disabled="" selected="" value="">Select Service</option>
								<option>Dental</option>
								<option>Dermatologist | Aesthetics</option>
							</select>
							<select name="doctor" class="intro_select intro_input" required="required">
								<option disabled="" selected="" value="">Select Doctor</option>
								<option>DR. LOKENDRA LIMBU</option>
								<option>DR. KHAGENDRA CHHETRI</option>
								<option>DR. SUNESHA PRADHAN</option>
								<option>DR. RAJEEV SINGH</option>
								<option>DR. RATNA BAHADUR GHARTI</option>
							</select>
							<input type="date" name="date" id="datepicker" class="intro_input datepicker hasDatepicker" placeholder="Date" required="required">
						</div>
						<button type="submit" class="button button_1 contact_button trans_200">make an appointment</button>
					</form>
				</div>
			</div>

			<!-- Contact Content -->
			<div class="col-lg-5 offset-lg-1 contact_col">
				<div class="contact_content">
					<div class="contact_content_title">Get in touch with us</div>
					<div class="contact_content_text">
						<p>This Is a center providing skin related procedures to suit your needs according to your skin.  Our aim Is to give you the flawless skin that you desire. </p>
					</div>
					<div class="direct_line d-flex flex-row align-items-center justify-content-start">
						<div class="direct_line_title text-center">Direct Line</div>
						<div class="direct_line_num text-center">+977-9840000121</div>
					</div>
					<div class="contact_info">
						<ul>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>Address</div>
								<div>Shop No. 201, 2nd Floor<br>Labim Mall, Lalitpur</div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>Phone</div>
								<div><a href="tel:+9779840000121">+977-9840000121</a> | <a href="tel:+977015551176">+977-015551176</a></div>
							</li>
							<li class="d-flex flex-row align-items-start justify-content-start">
								<div>E-mail</div>
								<div><a href="mailto:info@absoluteasthetics.com.np">info@absoluteaesthetics.com.np</a></div>
							</li>
						</ul>
					</div>
					<div class="contact_social">
						<ul class="d-flex flex-row align-items-center justify-content-start">
							<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
							<li><a href="https://www.facebook.com/absoluteaestheticsnepal/"><i class="fa fa-facebook" target="_blank" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="https://www.instagram.com/absoluteaestheticsnepal/"><i class="fa fa-instagram" target="_blank" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row google_map_row">
			<div class="col">
				
				<!-- Contact Map -->

				<div class="contact_map">

					<!-- Google Map -->
					
					<div class="map">
						<div id="google_map" class="google_map">
							<div class="map_container">
								<div id="map">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.2979391973636!2d85.31475751493022!3d27.67718498280447!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a9fddbda0b%3A0xdd0348355703bd6d!2sAbsolute+Aesthetics+Nepal!5e0!3m2!1sen!2snp!4v1556700504522!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>

<!-- Newsletter -->

<div class="newsletter">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="newsletter_title">Subscribe to our newsletter</div>
			</div>
		</div>
		<div class="row newsletter_row">
			<div class="col-lg-8 offset-lg-2">
				<div class="newsletter_form_container">
					<form action="#" id="newsleter_form" class="newsletter_form">
						<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection