@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/services.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/services_responsive.css')}}">
@section('content')

<style type="text/css">
	.social i {
     margin-top: 0px; 
}
</style>
<!-- Home -->

	<div class="home d-flex flex-column align-items-start justify-content-end">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/uploads/{{$services->image}}" data-speed="0.8"></div>
		<div class="home_overlay"><img src="{{asset('images/home_overlay.png')}}" alt=""></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div style="text-transform: uppercase; color: #ffff;" class="home_title">{{$services->name}}</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title_container">
						<div class="section_subtitle">This is Absolute Aesthetics</div>
						<div style="text-transform: capitalize;" class="section_title"><h2>{{$services->name}}</h2></div>
					</div>
				</div>
			</div>
			<div class="detail-image services_row">
				
				<!-- Service -->
				<div class="service_col">
					<div class="service text-center">
						<div class="service">
							<a href="">
							<div>
								<img style="max-width: 100%;" src="/uploads/{{$services->image}}">
							</div>
							<div class="service_text">
								<p><?php echo ($services->description)?></p>
							</div>
						</div>
					</a>
					</div>
				</div>


			</div>
		</div>
	</div>
	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="#" id="newsleter_form" class="newsletter_form">
							<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
							<button class="newsletter_button">subscribe</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection