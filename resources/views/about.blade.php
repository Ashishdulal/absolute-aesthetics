@extends('layouts.main.main')

@section('content')

<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/about.jpg" data-speed="0.8"></div>
	<div class="home_overlay"><img src="images/home_overlay.png" alt=""></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title">About us</div>
						<div class="home_text">Our aim Is to give you the flawless skin that you desire.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Intro -->

<div class="intro">
	<div class="container">
		<div class="row">

			<!-- Intro Content -->
			<div class="col-lg-8">
				<div class="intro_content">
					<div class="section_title_container">
						<div class="section_subtitle">This is Absolute Aesthetics</div>
						<div class="section_title"><h2>Welcome to our Clinic</h2></div>
					</div>
					<div class="intro_text">
						<p>Absolute Aesthetics is a specialized center geared towards achieving excellence regarding skin hair and dental aesthetics.Here we help you get your desired look with the help of trained and experienced dermatologists, dentists and plastic surgeons.This Is a center providing skin related procedures to suit your needs according to your skin. Along with skin procedure we also provide services regarding dental aesthetics. Flawless young and radiant skin along with a bright shining  smile Is a winning combination.</p>
					</div>

					<!-- Milestones -->
					<div class="milestones">
						<div class="row milestones_row">
							
							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="5000" data-sign-before="+">0</div>
									<div class="milestone_text">Satisfied Patients</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="352">0</div>
									<div class="milestone_text">Face Liftings</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="718">0</div>
									<div class="milestone_text">Injectibles</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="5">0</div>
									<div class="milestone_text">Awards Won</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>

			<!-- Intro Image -->
			<div class="col-lg-3 offset-lg-1">
				<div class="intro_image"><img src="images/intro_1.jpg" alt=""></div>
			</div>
		</div>
	</div>
</div>

<!-- Testimonials -->

<div class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle">This is Absolute Aesthetics</div>
					<div class="section_title"><h2>Clients Testimonials</h2></div>
				</div>
			</div>
		</div>
<div id="myCarousel" class="carousel slide" data-ride="carousel">

	<!-- Wrapper for carousel items -->
	<div class="carousel-inner">
		@foreach($testimonials as $testimonial)
		<div class="item carousel-item">
			<div class="img-box"><img src="/uploads/{{$testimonial->image}}" alt="hello"></div>
			<p class="testimonial"><?php echo ($testimonial->description ) ?></p>
			<p class="overview"><b>{{$testimonial->name}}</b>Patient</p>
		</div>
		@endforeach
		<div class="item carousel-item active">
			<div class="img-box"><img src="{{asset('/images/administrator.png')}}" alt="Administrator"></div>
			<p class="testimonial">kindly requesting you to write your opinion about our center and services : @ info@absoluteaesthetics.com.</p>
			<p class="overview"><b>Absolute Aesthetics</b>Admin</p>
		</div>	
	</div>
	<!-- Carousel controls -->
	<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
		<i class="fa fa-angle-left"></i>
	</a>
	<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
		<i class="fa fa-angle-right"></i>
	</a>
</div>
	</div>
</div>

<!-- Call to action -->

<div class="cta">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="cta_container d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
					<div class="cta_content">
						<div class="cta_title">Make your appointment today!</div>
						<div class="cta_text">Our aim Is to give you the flawless skin that you desire</div>
					</div>
					<div class="cta_phone ml-lg-auto"><a style="color: #fff;" href="tel:+9779840000121">+977-9840000121 </a> &nbsp; | <a style="color: #fff;" href="tel:+977015551176">+977-01-5551176</a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Team -->

<div class="team">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle">This is Absolute Aesthetics</div>
					<div class="section_title"><h2>Meet Our Doctors</h2></div>
				</div>
			</div>
		</div>
		<div class="row ">
			
			<!-- Team Item -->
			@foreach($doctors as $doctor)
			<a class="" data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">
				<div class="col-lg-12 team_col">
					<div class="team_item text-center d-flex flex-column aling-items-center justify-content-end">
						<div class="team_image"><img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}"></div>
						<div class="team_content text-center">
							<div class="team_name"><a data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">{{$doctor->name}}</a></div>
							<div class="team_title">{{$doctor->designation}}</div>
							<div class="team_text my-overflow">
								<p><?php echo ($doctor->description)?></p>
							</div>
							<div class=" button_1 trans_200 team-readmore"><a href="#" data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}">View Details</a></div>
						</div>
					</div>
				</div>
			</a>
			<div class="modal fade" id="team-membermodal{{$doctor->id}}" tabindex="-1" role="dialog" aria-labelledby="team-membermodal5Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Absolute Aesthetics</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}">
										</div>
										<ul class="portfolio-social">
											<li><a href="{{$doctor->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="{{$doctor->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="{{$doctor->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:{{$doctor->mail}}" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>{{$doctor->name}}</h4>
											<h2>{{$doctor->designation}}</h2>
											<p align="justify"><?php echo ($doctor->description)?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</div>

<!-- <div class="team">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle">This is Absolute Aesthetics</div>
					<div class="section_title"><h2>Meet Our Doctors</h2></div>
				</div>
			</div>
		</div>
		<div class="row ">
			

			@foreach($doctors as $doctor)
			<a class="" data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">
				<div class="col-lg-4 team_col">
					<div class="team_item text-center d-flex flex-column aling-items-center justify-content-end">
						<div class="team_image"><img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}"></div>
						<div class="team_content text-center">
							<div class="team_name"><a data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">{{$doctor->name}}</a></div>
							<div class="team_title">{{$doctor->designation}}</div>
							<div class="team_text my-overflow">
								<p><?php echo ($doctor->description)?></p>
							</div>
						</div>
					</div>
				</div>
			</a>
			<div class="modal fade" id="team-membermodal{{$doctor->id}}" tabindex="-1" role="dialog" aria-labelledby="team-membermodal5Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Absolute Aesthetics</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}">
										</div>
										<ul class="portfolio-social">
											<li><a href="{{$doctor->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="{{$doctor->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="{{$doctor->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:{{$doctor->mail}}" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>Dr. {{$doctor->name}}</h4>
											<h2>{{$doctor->designation}}</h2>
											<p align="justify"><?php echo ($doctor->description)?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</div>
 -->
<!-- Newsletter -->

<div class="newsletter">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="newsletter_title">Subscribe to our newsletter</div>
			</div>
		</div>
		<div class="row newsletter_row">
			<div class="col-lg-8 offset-lg-2">
				<div class="newsletter_form_container">
					<form action="#" id="newsleter_form" class="newsletter_form">
						<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection