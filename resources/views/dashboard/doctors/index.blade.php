@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->


								<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">
														<a href="/home/doctors">Doctors</a>
													</li>
												</ul>
											</div>
										</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/doctor/create" class="btn btn-primary">Add New Doctor</a>
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			
				<h3>Dermatology</h3>
			<table class="table table-borderless table-data3">
				<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Designation</th>
							<th>Description</th>
							<th>Image</th>
							<th>Links</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
							@foreach($doctor as $team)
							@if($team['category'] === 1)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{$team->name}}</td>
							<td>{{$team->designation}}</td>
							<td><?php echo ($team->description)?></td>
							<td class="work-img">
								<img src="/uploads/{{$team->image}}" alt="{{$team->name}}">
							</td>
							<td><span class="fab fa-facebook">&nbsp;: {{$team->facebook_link}}</span><br>
								<span class="fab fa-twitter">&nbsp;:{{$team->twitter_link}}</span><br>
								<span class="fab fa-instagram">&nbsp;:{{$team->instagram_link}}</span><br>
								<span class="fa fa-envelope">&nbsp;:{{$team->mail}}</span>
							</td>
							<td>
								<a href="doctor/edit/{{$team->id}}" class="btn btn-primary make-btn">
									Edit
								</a>|
								<form method="post" action="{{route('delete.doctor',$team->id)}}">
									@csrf
									{{ method_field('DELETE') }}
									<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
								</form>
							</td>
						</tr>
							@endif
							@endforeach
					</tbody>
			</table>&nbsp;
			
				<h3>Dental</h3>
			<table class="table table-borderless table-data3">
				<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Designation</th>
							<th>Description</th>
							<th>Image</th>
							<th>Links</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($doctor as $team)
			@if($team['category'] === 2)
						<tr>
							<td>{{$team->id}}</td>
							<td>{{$team->name}}</td>
							<td>{{$team->designation}}</td>
							<td><?php echo ($team->description)?></td>
							<td class="work-img">
								<img src="/uploads/{{$team->image}}" alt="{{$team->name}}">
							</td>
							<td><span class="fab fa-facebook">&nbsp;: {{$team->facebook_link}}</span><br>
								<span class="fab fa-twitter">&nbsp;:{{$team->twitter_link}}</span><br>
								<span class="fab fa-instagram">&nbsp;:{{$team->instagram_link}}</span><br>
								<span class="fa fa-envelope">&nbsp;:{{$team->mail}}</span>
							</td>
							<td>
								<a href="doctor/edit/{{$team->id}}" class="btn btn-primary make-btn">
									Edit
								</a>|
								<form method="post" action="{{route('delete.doctor',$team->id)}}">
									@csrf
									{{ method_field('DELETE') }}
									<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
								</form>
							</td>
						</tr>
						@endif
			@endforeach
					</tbody>
			</table>
			
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>

@endsection