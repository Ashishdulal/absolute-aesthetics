@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/doctors">Doctor</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">Edit</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
											<form action="/home/doctor/edit/{{$doctor->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
												@csrf
												<fieldset>
													<div class="form-group">
														<label class="col-md-3 control-label" for="image">Choose Doctors Category:</label>
														<div class="col-md-9">
															<select name="category" required class="form-control">
																@if($doctor->category === 1)
																<option value="1">Dermatology</option>
																<option value="2">Dental</option>
																@else()
																<option value="2">Dental</option>
																<option value="1">Dermatology</option>
																@endif
															</select>
														</div>
													</div>
													<!-- Name input-->
													<div class="form-group">
														<label class="col-md-3 control-label" for="name">Doctor Name:</label>
														<div class="col-md-9">
															<input id="name" name="name" type="text" value="{{$doctor->name}}"  class="form-control">
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label" for="designation">Doctor Designation:</label>
														<div class="col-md-9">
															<input id="designation" name="designation" type="text" value="{{$doctor->designation}}"  class="form-control">
														</div>
													</div>						
													<div class="form-group">
														<label class="col-md-3 control-label" for="description">Description:</label>
														<div class="col-md-9">
															<textarea id="description" name="description" type="text" required=""  class="form-control">{{$doctor->description}}</textarea>
														</div>
													</div>
													<!-- image input-->
													<div class="form-group col-md-6">
														<app-image-upload  >
															<div  class="fileinput">
																<div  class="thumbnail img-raised">
																	<img src="/uploads/{{$doctor->image}}" alt="{{$doctor->name }}"/>
																</div>
																<div >
																	<label class="control-label" for="image">Select Image:</label>
																</div>
																<input  type="file" name="image"  accept="image/png, image/jpg, image/jpeg">
															</div>
														</app-image-upload>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label" for="facebook_link">Facebook Link:</label>
														<div class="col-md-9">
															<input id="facebook_link" name="facebook_link" required="" type="text" value="{{$doctor->facebook_link}}" class="form-control">
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label" for="twitter_link">Twitter Link:</label>
														<div class="col-md-9">
															<input id="twitter_link" name="twitter_link" type="text" required="" value="{{$doctor->twitter_link}}" class="form-control">
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label" for="instagram_link">Instagram Link:</label>
														<div class="col-md-9">
															<input id="instagram_link" name="instagram_link" type="text" required="" value="{{$doctor->instagram_link}}" class="form-control">
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label" for="mail">Mail Id:</label>
														<div class="col-md-9">
															<input id="mail" name="mail" type="email" required="" value="{{$doctor->mail}}" class="form-control">
														</div>
													</div>

													<!-- Form actions -->
													<button type="submit" class="btn btn-primary btn-sm">
														<i class="fa fa-dot-circle-o"></i> Submit
													</button>
													<button type="reset" class="btn btn-danger btn-sm">
														<i class="fa fa-ban"></i> Reset
													</button>
												</fieldset>
											</form>
										</div>
										<div class="card-footer">

										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection