<?php

namespace App\Http\Controllers;

use App\Testimonial;
use App\Doctors;
use App\Slider;
use Carbon\Carbon;
use App\Blog;
use App\Blogcategory;
use App\Gallery;
use App\Service;
use App\Mainpage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\view_count; 
use Session;

class MainpageController extends Controller 
{

   public function index(){
    $viewCount = view_count::firstOrCreate([
      'ip' => $_SERVER['REMOTE_ADDR'],
      'session_id' => session()->getID()
    ]);

    $result = view_count::where('session_id', session()->getID())->first();

    if($result){

      $result->view_count += 1;
      $result->save();
    }

   	$services = Service::latest()->get();
   	$gallery = Gallery::latest()->get();
    $sliders = Slider::latest()->get();
   	return view('index',compact('services', 'gallery','sliders'));
   }

   public function aboutPage(){
    $doctors = Doctors::all();
    $testimonials = Testimonial::latest()->get();;
   return view('/about', compact('doctors','testimonials'));
  }

   public function galleryPage(){
   	$gallery = Gallery::latest()->get();
   return view('gallery', compact('gallery'));
  }

	public function blogdetail($id)
    {
        $blogs = Blog::findOrFail($id);
        $blogcategories = Blogcategory::latest()->get();
        return view ('blog-detail',compact('blogs','blogcategories'));
    }
    public function bloghomepage()
    {
        $blogs = Blog::latest()->paginate(4);
        $blogcategories = Blogcategory::latest()->get();
        return view ('blog',compact('blogs','blogcategories'));
    }
    public function servicepage()
    {
        $services = Service::latest()->paginate(6);
        return view('services', compact('services'));
    }
    public function detailpage(Service $service,$id)
    {
        $services = Service::findOrFail($id);
        return view('service-detail', compact('services'));
    }
}