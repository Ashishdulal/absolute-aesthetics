<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;	
use App\Mail\SendMail;

class SendMailController extends Controller
{
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'service' =>  'required',
      'phone' =>  'required',
      'doctor' =>  'required',
      'date' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'service'   =>   $request->service,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'doctor'   =>   $request->doctor,
            'date'   =>   $request->date
        );
        
        $txt1 = '<html>
        <head>  
        </head>
        <body>
                    <p>Hi, This is '. $data['name'] .'</p>
                    <p>'. $data['email'] .'<br><br>'. $data['phone'] .'</p>
                    <p>I have query for the '. $data['service'] .' service with '. $data['doctor'] .' in the date '. $data['date'] .'.</p>
                    <p>It would be appriciative, if i receive the appointment soon.</p>
        </body>
        </html>';       

        $to = "info@absoluteaesthetics.com.np";
        $subject = "Appointment Request";

        $headers = "From:absoluteaesthetics.com.np\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


         $result=   mail($to,$subject,$txt1,$headers);
                 return back()->with('success','Thanks for contacting us!');
        }


    




}
