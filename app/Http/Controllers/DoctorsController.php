<?php

namespace App\Http\Controllers;

use App\Doctors;
use Illuminate\Http\Request;

class DoctorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctor = Doctors::all();
        return view('dashboard.doctors.index', compact('doctor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.doctors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $doctor = new Doctors();
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $doctor->category =$request->category;
        $doctor->name =$request->name;
        $doctor->designation =$request->designation;
        $doctor->description =$request->description;
        $doctor->facebook_link =$request->facebook_link;
        $doctor->twitter_link =$request->twitter_link;
        $doctor->instagram_link =$request->instagram_link;
        $doctor->mail =$request->mail;
        if(file_exists($request->file('image'))){
            $image = "doctor".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $doctor->image = $image;
        }
        else{
            $doctor->image = 'default-thumbnail.png';
        }
        $doctor->save();
        return redirect('/home/doctors');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Doctors  $Doctors
     * @return \Illuminate\Http\Response
     */
    public function show(Doctors $Doctors)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doctors  $Doctors
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctors $Doctors, $id)
    {
        $doctor = Doctors::findOrFail($id);
        return view ('dashboard.doctors.edit',compact('doctor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctors  $Doctors
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctors $Doctors,$id)
    {
        $doctor = Doctors::findOrFail($id);
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg'
        ]);
        $doctor->category =$request->category;
        $doctor->name =$request->name;
        $doctor->designation =$request->designation;
        $doctor->description =$request->description;
        $doctor->facebook_link =$request->facebook_link;
        $doctor->twitter_link =$request->twitter_link;
        $doctor->instagram_link =$request->instagram_link;
        $doctor->mail =$request->mail;
        if(file_exists($request->file('image'))){
            $image = "doctor".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $doctor->image = $image;
        }
        else{
            $doctor->image = $doctor->image;
        }
        $doctor->save();
        return redirect('home/doctors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctors  $Doctors
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $doctor = Doctors::findOrFail($id)->delete();
    return redirect()->back();
    }
}
