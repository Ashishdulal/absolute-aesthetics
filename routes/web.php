<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/contact', function () {
    return view('contact');
});

Route::fallback(function() {
    return view('nopage');
});


Route::get('/','MainpageController@index');

Route::post('/sendemail/send', 'SendMailController@send');



Route::get('/home/services', 'ServiceController@index');
Route::get('/home/services/create', 'ServiceController@create');
Route::post('/home/services/create', 'ServiceController@store');
Route::get('/home/services/edit/{id}', 'ServiceController@edit')->name('service.edit');
Route::post('/home/services/edit/{id}', 'ServiceController@update')->name('service.update');
Route::delete('/home/services/destroy/{id}', 'ServiceController@destroy')->name('service.delete');



Route::get('/show-gallery', 'MainpageController@galleryPage');

Route::get('/home/gallery', 'GalleryController@index');
Route::get('/home/gallery/create', 'GalleryController@create');
Route::post('/home/gallery/create', 'GalleryController@store');
Route::get('/home/gallery/createVideo', 'GalleryController@createVideo');
Route::post('/home/gallery/createVideo', 'GalleryController@storeVideo');
Route::get('/home/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/home/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/home/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');


Route::get('/home/blog-category', 'BlogcategoryController@index');
Route::get('/home/blog-category/create', 'BlogcategoryController@create');
Route::post('/home/blog-category/create', 'BlogcategoryController@store')->name('make.blog');
Route::get('/home/blog-category/{id}', 'BlogcategoryController@show');
Route::get('/home/blog-category/edit/{id}', 'BlogcategoryController@edit');
Route::post('/home/blog-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
Route::get('/home/blog-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');

Route::get('/services', 'MainpageController@servicepage');
Route::get('/services-detail/{id}', 'MainpageController@detailpage');

Route::get('/blog', 'MainpageController@bloghomepage');
Route::get('/blog-detail/{id}', 'MainpageController@blogdetail');

Route::get('/home/blogs', 'BlogController@index');
Route::get('/home/blog/create', 'BlogController@create');
Route::post('/home/blog/create', 'BlogController@store')->name('make-post.blog');
Route::get('/home/blog/{id}', 'BlogController@show');
Route::get('/home/blog/edit/{id}', 'BlogController@edit');
Route::post('/home/blog/edit/{id}', 'BlogController@update')->name('update-post.blog');
Route::get('/home/blog/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');


Route::get('/home/sliders', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');


Route::get('/about', 'MainpageController@aboutPage');



Route::get('/home/doctors', 'DoctorsController@index');
Route::get('/home/doctor/create', 'DoctorsController@create');
Route::post('/home/doctor/create', 'DoctorsController@store')->name('make.doctor');
Route::get('/home/doctor/{id}', 'DoctorsController@show');
Route::get('/home/doctor/edit/{id}', 'DoctorsController@edit');
Route::post('/home/doctor/edit/{id}', 'DoctorsController@update')->name('update.doctor');
Route::delete('/home/doctor/destroy/{id}', 'DoctorsController@destroy')->name('delete.doctor');


Route::get('/home/testimonials', 'TestimonialController@index');
Route::get('/home/testimonials/create', 'TestimonialController@create');
Route::post('/home/testimonials/create', 'TestimonialController@store');
Route::get('/home/testimonials/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/home/testimonials/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/home/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');